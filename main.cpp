#include <iostream>
#include <stdio.h>
#include <vector>

using namespace std;

int main(void)
{
    const int MAX_SIZE = 100;
	int graph[MAX_SIZE][MAX_SIZE] = {0};
	FILE *inputFile = fopen("input.txt", "r");
	int index1, index2, price, maxIndex = 0;
	if (! inputFile) {
		cerr << "Error at reading file" << endl;
		return 1;
	}
    int edgesCount;
    fscanf(inputFile, "%d", &edgesCount);
    for (int i = 0; i < edgesCount; ++i) {
        fscanf(inputFile, "%d %d %d", &index1, &index2, &price);
		graph[index1-1][index2-1] = price;
		graph[index2-1][index1-1] = price;
		if (index1 > maxIndex)
			maxIndex = index1;
		if (index2 > maxIndex)
			maxIndex = index2;
	}
    vector<int> vertices;
    vector<int> treeBranches;
    vertices.push_back(0);
    for (int i = 0; i < maxIndex; ++i) {
        graph[i][0] = 0;
    }
    while (vertices.size() < maxIndex) {
        vector<int> candidates;
        vector<int> prices;
        vector<int> fromWhere;
        for (int i = 0; i < vertices.size(); ++i) {
            int current = vertices[i];
            for (int j = 0; j < maxIndex; ++j)
                if (graph[current][j] != 0) {
                    candidates.push_back(j);
                    fromWhere.push_back(current);
                    prices.push_back(graph[current][j]);
                }
        }
        int minPrice = prices[0];
        int minIndex = candidates[0];
        int minFromWhere = fromWhere[0];
        for (int i = 1; i < prices.size(); ++i) {
            if (prices[i] < minPrice) {
                minPrice = prices[i];
                minIndex = candidates[i];
                minFromWhere = fromWhere[i];
            }
        }
        vertices.push_back(minIndex);
        for (int i = 0; i < maxIndex; ++i) {
            graph[i][minIndex] = 0;
        }
        treeBranches.push_back(minIndex+1);
        treeBranches.push_back(minFromWhere+1);
    }
    for (int i = 0; i < treeBranches.size(); i+=2)
        cout << treeBranches[i] << ' ' << treeBranches[i+1] << endl;
	fclose(inputFile);
	return 0;
}
